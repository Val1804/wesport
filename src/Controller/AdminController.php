<?php

namespace App\Controller;

use App\Entity\Home;
use App\Entity\User;
use App\Entity\Sport;
use App\Form\HomeType;
use App\Form\UserType;
use App\Form\SportType;
use App\Entity\UserInfo;
use App\Entity\EventSport;
use App\Form\EventSportType;
use App\Form\AdminEditUserType;
use App\Repository\HomeRepository;
use App\Repository\UserRepository;
use App\Repository\SportRepository;
use App\Repository\UserInfoRepository;
use App\Repository\EventSportRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminController extends AbstractController
{
    // Accueil interface administrateur

    /**
     * @Route("/admin", name="admin")
     */
    public function index(EventSportRepository $repo)
    {
        $eventSport = $repo->findAll();
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
            'eventSport' => $eventSport
        ]);
    }

    // Editer un événement en tant qu'admnistrateur

    /**
     * @Route("/admin/events/{id}/edit", name="admin_edit_event_sport")
     */
    public function admin_edit_event_sport(EventSport $eventSport, Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createForm(EventSportType::class, $eventSport);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$eventSport->getId()) {
                $eventSport->setCreatedAt(new \DateTime());
            }

            $manager->persist($eventSport);
            $manager->flush();
            $this->addFlash('success', 'Modifié avec succès');

            return $this->redirectToRoute('admin');
        }

        return $this->render(
            'admin/admin_edit_event_sport.html.twig',
            [
                'eventSport' => $eventSport,
                'form' => $form->createView()
            ]
        );
    }

    // Voir un événement en tant qu'administrateur

    /**
     * @Route("/admin/events/{id}/show", name="admin_show_event_sport")
     */
    public function admin_show(EventSport $eventSport)
    {
        return $this->render(
            'admin/admin_show_event_sport.html.twig',
            [
                'eventSport' => $eventSport
            ]
        );
    }


    // Supprimer un événement en tant qu'administrateur

    /**
     * @Route("/admin/event_sport/{id}", name="admin_delete_event_sport")
     */
    public function delete(EventSport $eventSport, EntityManagerInterface $manager, Request $request)
    {
        if ($this->isCsrfTokenValid('delete' . $eventSport->getId(), $request->get('_token'))) {
            $manager->remove($eventSport);
            $manager->flush();
            $this->addFlash('success', 'Supprimé avec succès');
        }

        return $this->redirectToRoute('admin');
    }











    /**
     * @Route("/admin/sports", name="admin_show_sports")
     */
    public function show_sports(SportRepository $repo)
    {
        $sports = $repo->findAll();
        return $this->render('admin/admin_show_sports.html.twig', [
            'sports' => $sports
        ]);
    }

    /**
     * @Route("/admin/sport/add", name="admin_add_sport")
     */
    public function admin_add_sport(Request $request, EntityManagerInterface $manager)
    {

        $sport = new Sport();
        $form = $this->createForm(SportType::class, $sport);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($sport);
            $manager->flush();
            return $this->redirectToRoute('admin_show_sports');
        }

        return $this->render(
            'admin/admin_add_sport.html.twig',
            [
                'sport' => $sport,
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/admin/sport/{id}/edit", name="admin_sport_edit")
     */
    public function admin_sport_edit(Sport $sport, Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createForm(SportType::class, $sport);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $manager->persist($sport);
            $manager->flush();
            $this->addFlash('success', 'Modifié avec succès');

            return $this->redirectToRoute('admin_show_sports');
        }

        return $this->render(
            'admin/admin_sport_edit.html.twig',
            [
                'sport' => $sport,
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/admin/sport/{id}", name="admin_sport_delete")
     */
    public function delete_sport(Sport $sport, EntityManagerInterface $manager, Request $request)
    {
        if ($this->isCsrfTokenValid('deletesport' . $sport->getId(), $request->get('_token'))) {
            $manager->remove($sport);
            $manager->flush();
            $this->addFlash('success', 'Supprimé avec succès');
        }

        return $this->redirectToRoute('admin_show_sports');
    }










    /**
     * @Route("/admin/users", name="admin_show_users")
     */
    public function show_users(UserInfoRepository $repo)
    {
        $userInfos = $repo->findByUsers();
        // dd($userInfos);
        return $this->render('admin/admin_show_users.html.twig', [
            'userInfos' => $userInfos
        ]);
    }

    /**
     * @Route("/admin/user/{id}/edit", name="admin_user_edit")
     */
    public function admin_user_edit(User $user, Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createForm(AdminEditUserType::class, $user);

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($user);
            $manager->flush();
            $this->addFlash('success', 'Modifié avec succès');

            return $this->redirectToRoute('admin_show_users');
        }

        return $this->render(
            'admin/admin_user_edit.html.twig',
            [
                'user' => $user,
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/admin/user/{id}", name="admin_user_delete")
     */
    public function user_delete(User $user, UserInfo $userInfo, EntityManagerInterface $manager, Request $request)
    {
        if ($this->isCsrfTokenValid('deleteuser' . $userInfo->getId(), $request->get('_token'))) {
            $manager->remove($userInfo);
            $manager->flush();
            $this->addFlash('success', 'Supprimé avec succès');
        }

        return $this->redirectToRoute('admin_show_users');
    }












    /**
     * @Route("/admin/home/edit/{id}", name="admin_home_edit")
     */
    public function admin_home_edit(Home $home, Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createForm(HomeType::class, $home);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $manager->persist($home);
            $manager->flush();
            $this->addFlash('success', 'Modifié avec succès');

            return $this->redirectToRoute('admin');
        }

        return $this->render(
            'admin/home_create.html.twig',
            [
                'home' => $home,
                'form' => $form->createView()
            ]
        );
    }
}
