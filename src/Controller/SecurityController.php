<?php

namespace App\Controller;

use App\Form\ResetPassType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/security", name="security")
     */
    public function index()
    {
        return $this->render('security/index.html.twig', [
            'controller_name' => 'SecurityController',
        ]);
    }

    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    /**
     * @Route("/oubli-pass", name="app_forgotten_password")
     */
    public function forgottenPass(Request $request, UserRepository $usersRepo, EntityManagerInterface $manager, \Swift_Mailer $mailer, TokenGeneratorInterface $tokenGenerator)
    {
        $form = $this->createForm(ResetPassType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $donnees = $form->getData();
            $user = $usersRepo->findOneByEmail($donnees['email']);
            if (!$user) {
                $this->addFlash('danger', 'Cette adresse n\'existe pas');
                return $this->redirectToRoute('login');
            }
            $token = $tokenGenerator->generateToken();
            try {
                $user->setResetToken($token);
                $manager->persist($user);
                $manager->flush();
            } catch (\Exception $e) {
                $this->addFlash('warning', 'Une erreur est survenue: ' . $e->getMessage());
                return $this->redirectToRoute('login');
            }

            $url = $this->generateUrl(
                'app_reset_password',
                [
                    'token' => $token
                ],
                UrlGeneratorInterface::ABSOLUTE_URL
            );

            $message = (new \Swift_Message('Mot de passe oublié'))
                ->setFrom('no-reply@wesport.fr')
                ->setTo($user->getEmail())
                ->setBody("Bonjour, <br><br> Une demande de réinitialisation de mot de passe 
                a été effectuée pour le site WeSport.fr. Veuillez cliquer sur le lien suivant: " . $url, "text/html");
            $mailer->send($message);

            $this->addFlash('message', 'Un email de réinitialisatio de mot de passe vous a été envoyé');
            return $this->redirectToRoute('login');
        }
        return $this->render('security/forgotten_password.html.twig', [
            'emailForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/reset-pass/{token}", name="app_reset_password")
     */
    public function resetPassword($token, Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $passwordEncoder, UserRepository $user_repo)
    {
        $user = $user_repo->findOneBy(['reset_token' => $token]);
        if (!$user) {
            $this->addFlash('danger', 'Token inconnu');
            return $this->redirectToRoute('login');
        }
        if ($request->isMethod('POST')) {
            $user->setResetToken(null);
            $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('password')));
            $manager->persist($user);
            $manager->flush();

            $this->addFlash('message', 'Mot de passe modifié avec succès');

            return $this->redirectToRoute('login');
        } else {
            return $this->render('security/reset_password.html.twig', ['token' => $token]);
        }
    }
}
