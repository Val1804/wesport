<?php

namespace App\DataFixtures;

use App\Entity\EventSport;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i < 10; $i++) {
            $sportEvent = new EventSport();
            $sportEvent->setTitle("Titre de l'événement n°$i")
                ->setImage("http://placehold.it/350x150")
                ->setDescriptionEvent("description de l'événement n°$i")
                ->setEventDate(new \DateTime())
                ->setCreatedAt(new \DateTime())
                ->setZipCode("0790")
                ->setCity("St Sauveur de Montagut");
            $manager->persist($sportEvent);
        }
        $manager->flush();
    }
}
