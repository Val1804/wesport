<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\HomeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HomeRepository::class)
 */
class Home
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(min=2,minMessage = "La description est trop courte")
     */
    private $teaser;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(min=2,minMessage = "La description est trop courte")
     */
    private $descriptionHome;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(min=2,minMessage = "La description est trop courte")
     */
    private $whatweare;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(min=2,minMessage = "La description est trop courte")
     */
    private $whojoinus;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(min=2,minMessage = "La description est trop courte")
     */
    private $mantra;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTeaser(): ?string
    {
        return $this->teaser;
    }

    public function setTeaser(string $teaser): self
    {
        $this->teaser = $teaser;

        return $this;
    }

    public function getDescriptionHome(): ?string
    {
        return $this->descriptionHome;
    }

    public function setDescriptionHome(string $descriptionHome): self
    {
        $this->descriptionHome = $descriptionHome;

        return $this;
    }

    public function getWhatweare(): ?string
    {
        return $this->whatweare;
    }

    public function setWhatweare(string $whatweare): self
    {
        $this->whatweare = $whatweare;

        return $this;
    }

    public function getWhojoinus(): ?string
    {
        return $this->whojoinus;
    }

    public function setWhojoinus(string $whojoinus): self
    {
        $this->whojoinus = $whojoinus;

        return $this;
    }

    public function getMantra(): ?string
    {
        return $this->mantra;
    }

    public function setMantra(string $mantra): self
    {
        $this->mantra = $mantra;

        return $this;
    }
}
