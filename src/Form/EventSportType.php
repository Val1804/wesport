<?php

namespace App\Form;

use App\Entity\Sport;
use App\Entity\EventSport;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class EventSportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('image', TextType::class)
            ->add('description_event', TextareaType::class)
            ->add('eventDate', DateTimeType::class, [
                'widget' => 'choice',
                'format' => 'yyyy-MM-dd',
                'html5' => false,
                'years' => array(2020, 2021, 2022, 2023)
            ])
            // ->add('author', TextType::class)
            ->add('zipCode', TextType::class)
            ->add('city', TextType::class)
            ->add(
                'sport',
                EntityType::class,
                [
                    'class' => Sport::class,
                    'choice_label' => 'sportName',
                    'expanded' => false,
                    'multiple' => false
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EventSport::class,
        ]);
    }
}
