<?php

namespace App\Form;

use App\Entity\User;
use App\Form\UserInfoType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, ['disabled' => $options['is_edit']])
            ->add('email', TextType::class, ['disabled' => $options['is_edit']])
            ->add(
                'userInfo',
                UserInfoType::class,
                [
                    'by_reference' => false,
                    'label' => false
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'is_edit' => true,
        ]);
    }
}
