<?php

namespace App\Repository;

use App\Entity\EventSport;
use App\Entity\EventSportSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EventSport|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventSport|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventSport[]    findAll()
 * @method EventSport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventSportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventSport::class);
    }

    // Trouver les trois derniers événements mis en ligne
    public function findLastThree()
    {
        return $this->createQueryBuilder('e')
            ->orderBy('e.id', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();
    }

    // Trouver les événements en fonction de la recherche du sport ou du lieu
    public function findAllEventSport($search)
    {
        // dd($search->getTypeSport());
        if ($search->getTypeSport()) {
            $query = $this->createQueryBuilder('e')
                ->leftJoin('e.sports', 'sport')
                ->andWhere('sport.sportName = :sp')
                ->setParameter('sp', $search->getTypeSport());
        }
        if ($search->getCodePostal()) {
            $query = $query
                ->orderBy('e.id', 'DESC')
                ->andWhere('e.zipCode = :code')
                ->setParameter('code', $search->getCodePostal());
        }
        return $query->getQuery()
            ->getResult();
    }

    // /**
    //  * @return EventSport[] Returns an array of EventSport objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EventSport
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
